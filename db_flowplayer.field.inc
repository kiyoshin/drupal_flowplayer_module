<?php

/**
 * Implements hook_field_info().
 */
 
function db_flowplayer_field_info() {
  return array(
    'flowplayer' => array(
      'label' => t('Flowplayer'),
      'description' => t('This field stores the ID of an image file as an integer value.'),
        'settings' => array(
          'uri_scheme' => variable_get('file_default_scheme', 'public'),
          'default_image' => 0,
          'file_directory' => '',
          'video_base_link' => '',
          'video_event_collection' => 'No',
        ),
        'instance_settings' => array(
        // 'file_extensions' => 'png gif jpg jpeg',
        // 'file_directory' => '',
        // 'max_filesize' => '',
        // 'alt_field' => 0,
        // 'title_field' => 0,
        // 'max_resolution' => '',
        // 'min_resolution' => '',
        // 'default_image' => 0,
        ),
      'default_widget' => 'flowplayer_standard',
      'default_formatter' => 'flowplayer_default',
    ),
  );
}



/**
 * Implements hook_field_widget_info().
 */

function db_flowplayer_field_widget_info() {
  return array(
    'flowplayer_standard' => array(
      'label' => t('Flowplayer'),
      'field types' => array('flowplayer'),
    ),
  );
}

/**
 * Implements hook_field_widget_settings_form().
 */

function db_flowplayer_field_settings_form($field, $instance) {
  $defaults = field_info_field_settings($field['type']);
  $settings = array_merge($defaults, $field['settings']);

  $scheme_options = array();
  foreach (file_get_stream_wrappers(STREAM_WRAPPERS_WRITE_VISIBLE) as $scheme => $stream_wrapper) {
    $scheme_options[$scheme] = $stream_wrapper['name'];
  }
  $form['uri_scheme'] = array(
    '#type' => 'radios',
    '#title' => t('Upload destination'),
    '#options' => $scheme_options,
    '#default_value' => $settings['uri_scheme'],
    '#description' => t('Select where the final files should be stored. Private file storage has significantly more overhead than public files, but allows restricted access to files within this field.'),
  );

  $form['default_image'] = array(
    '#title' => t('Default image'),
    '#type' => 'managed_file',
    '#description' => t('If no image is uploaded, this image will be shown on display.'),
    '#default_value' => $field['settings']['default_image'],
    '#upload_location' => $settings['uri_scheme'] . '://default_images/',
  );
  
  $form['file_directory'] = array(
    '#title' => t('File Directory'),
    '#type' => 'textfield',
    '#description' => t('Optional subdirectory within the upload destination where files will be stored. Do not include preceding or trailing slashes.'),
    '#default_value' => $settings['file_directory'],
  );
  
  $form['video_base_link'] = array(
    '#title' => t('Video Base Link'),
    '#type' => 'textfield',
    '#default_value' => $field['settings']['video_base_link'],
    '#size' => 60,
    '#maxlength' => 120,
    '#required' => TRUE,
  );
  
  $form['video_event_collection'] = array(
    '#title' => t('Video Event Collection'),
    '#type' => 'radios',
    '#options' => drupal_map_assoc(array(t('Yes'), t('No'))),
    '#default_value' => $field['settings']['video_event_collection'],
  );
  
  return $form;
}

 
/**
 * Implements hook_field_widget_form().
 */
 
function db_flowplayer_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
   
   $setting = $field['settings'];
   
   $element['video_file'] = array(
    '#title' => t('Video File'),
     '#type' => 'textfield',
     '#size' => 30,
     '#maxlength' => 100,
     '#default_value' => (isset($items[$delta]['video_file']) ? $items[$delta]['video_file'] : ''),
   );
   
   $element['video_thumbnail_image'] = array(
     '#title' => t('Video thumbnail'),
     '#type' => 'managed_file',
     '#default_value' => (isset($items[$delta]['video_thumbnail_image']) ? $items[$delta]['video_thumbnail_image'] : ''),
     '#upload_location' => 'public://' . $setting['file_directory'],     
     '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
     ),
   );
   
   $element['video_share'] = array(
     '#title' => t('Video share'),
     '#type' => 'radios',
     '#options' => drupal_map_assoc(array(t('Yes'), t('No'))),
     '#default_value' => (isset($items[$delta]['video_share']) ? $items[$delta]['video_share'] : 'No'),
   );
  
  return $element;
}

/**
 * Implements hook_field_update().
 */
function db_flowplayer_field_update($entity_type, $entity, $field, $instance, $langcode, &$items) { 
  foreach ($items as $item => $value) {
    $old_fid = $entity->original->field_flowplayer['und'][$item]['video_thumbnail_image'];
    $new_fid = $items[$item]['video_thumbnail_image'];
    if($new_fid != $old_fid) {
      if($old_fid != 0) {
        $old_file = file_load($old_fid);
        file_delete($old_file, true);
      }
    }   
    if($value['video_thumbnail_image'] != 0) {
      $file = file_load($value['video_thumbnail_image']);
      if (!$file->status) {
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        file_usage_add($file, 'image', 'default_image', $instance['id']);
      }
    }
  }
}

/**
 * Implements hook_field_validate().
 */
function db_flowplayer_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  //at this point we will not validate anything, but will revisit
}

/**
 * Implements hook_field_insert().
 */
function db_flowplayer_field_insert($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $item => $value) {
    if($value['video_thumbnail_image'] != 0) {
      $file = file_load($value['video_thumbnail_image']);
      if (!$file->status) {
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        file_usage_add($file, 'image', 'default_image', $instance['id']);
      }
    }
  }
}

/**
 * Implements hook_field_delete().
 */
function db_flowplayer_field_delete($entity_type, $entity, $field, $instance, $langcode, &$items) {
  foreach ($items as $item => $value) {
    if($value['video_thumbnail_image'] != 0) {
      $file = file_load($value['video_thumbnail_image']);
      file_delete($file, true);
    }
  } 
}

/**
 * Implements hook_field_delete_revision().
 */
function db_flowplayer_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, &$items) {
//  file_field_delete_revision($entity_type, $entity, $field, $instance, $langcode, $items);
}

/**
 * Implements hook_field_is_empty().
 */
function db_flowplayer_field_is_empty($item, $field) {
//  return file_field_is_empty($item, $field);
}

/**
 * Implements hook_field_formatter_info().
 */
function db_flowplayer_field_formatter_info() {
  return array(
    'flowplayer_default' => array(
      'label' => t('Flowplayer (Digital Bytes)'),
      'field types' => array('flowplayer'),
      'settings' => array(
        'video_height' => 180,
        'video_width' => 320,
      ),
    ),
    'flowplayer_thumbnail' => array(
      'label' => t('Flowplayer (Digital Bytes) - thumbnail'),
      'field types' => array('flowplayer'),
      'settings' => array(
        'thumbnail_image_style' => '',
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_setting().
 */
function db_flowplayer_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $formatter = $display['type'];
  switch ($formatter) {
    case 'flowplayer_default':
      $element = array();
      $element['video_width'] = array(
        '#type'           => 'textfield',                        // Use a select box widget
        '#title'          => t('Video Width'),                   // Widget label
        '#description'    => t('Select what size of video width'), // Helper text
        '#default_value'  => $settings['video_width'],              // Get the value if it's already been set
        '#size' => 3,
        '#maxlength' => 3,
      );
      $element['video_height'] = array(
        '#type'           => 'textfield',                           // Use a select box widget
        '#title'          => t('Video Height'),                   // Widget label
        '#description'    => t('Select what size of video height'), // Helper text
        '#default_value'  => $settings['video_height'],              // Get the value if it's already been set
        '#size' => 3,
        '#maxlength' => 3,
      );
      break;
    case 'flowplayer_thumbnail':
      $image_styles = image_style_options(FALSE);
      $element = array();
      $element['thumbnail_image_style'] = array(
        '#title' => t('Thumbnail image style'),
        '#type' => 'select',
        '#default_value' => $settings['thumbnail_image_style'],
        '#empty_option' => t('None (original image)'),
        '#options' => $image_styles,
      );
      break;
  }
  return $element;
}

function db_flowplayer_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $formatter = $display['type'];
  switch ($formatter) {
    case 'flowplayer_default':
      $summary = array();
      $summary = t('Size of video player is @width width x @height height', array(
        '@width'    => $settings['video_width'],
        '@height'   => $settings['video_height'],
      ));
      break;
    case 'flowplayer_thumbnail':
      if(empty($settings['thumbnail_image_style'])) {
        $thumbnail_style = 'Original';
      } else {
        $thumbnail_style = $settings['thumbnail_image_style'];
      }
      $summary = t('Image Style is @image_style', array(
        '@image_style'    => $thumbnail_style,
      ));
      break;
  }
  return $summary;
}

function db_flowplayer_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $settings = $display['settings']; // get the settings
  $element = array(); // Initialize the var
  switch ($display['type']) {
    case 'flowplayer_default':
      $flowplayer_path = libraries_get_path('flowplayer');
      if (arg(0) == 'node' && is_numeric(arg(1))) $nid = arg(1);
  
      $default_image_file = file_load($field['settings']['default_image']); // get defaul image file object
      if(!empty($default_image_file)){
        $default_image = image_load($default_image_file->uri); // get defaul image object
        image_gd_resize($default_image, $settings['video_width'], $settings['video_height']);  // modify image size to the size of flowplayer
        image_save($default_image, 'public://default_images/resized/' . $default_image_file->filename);  // save modified image to "default_images/resized/..."
        $default_image_url = file_create_url('public://default_images/resized/' . $default_image_file->filename);
      } else {
        $default_image_url = "";
      }
  
      drupal_add_js('https://www.google.com/jsapi', 'external');
      //drupal_add_js('http://d7module.local/sites/all/modules/custom/db_flowplayer/google_chart.js');
      drupal_add_js($flowplayer_path . '/' . variable_get('flowplayer_javascript', ''));
      if(variable_get('flowplayer_ipad_compatibility', 'No') == 'Yes') {
        drupal_add_js($flowplayer_path . '/' . variable_get('flowplayer_ipad_plugin', ''));
      }
      foreach ($items as $delta => $item) {
        $video_thumbnail_file = file_load($item['video_thumbnail_image']);
        $video_thumbnail_url = !empty($video_thumbnail_file) ? file_create_url($video_thumbnail_file->uri) : $default_image_url;
        $video_url = $field['settings']['video_base_link'] . $item['video_file'];
        $video_type = variable_get('flowplayer_type', 'Free');
        $video_commerical_key = variable_get('flowplayer_commercial_key', '');
		$video_share = $item['video_share'];
        $flowplayer_js = flowplayer_create($video_url, $video_thumbnail_url, $video_type, $video_commerical_key, $video_share, variable_get('flowplayer_ipad_compatibility', 'No'), $nid, $field, $delta);
    	
    	if ($video_share == 'Yes') {;
		
			$tag_element = array(
				'#tag' => 'meta',
				'#attributes' => array(
					'property' => 'og:image',
					'content' => $video_thumbnail_url,
				),
			);
			drupal_add_html_head($tag_element, 'og_image');
		
			$tag_element = array(
				'#tag' => 'meta',
				'#attributes' => array(
					'property' => 'og:video:height',
					'content' => 360,
				),
			);
			drupal_add_html_head($tag_element, 'og_video_height');
		
			$tag_element = array(
				'#tag' => 'meta',
				'#attributes' => array(
					'property' => 'og:video:width',
					'content' => 640,
				),
			);
			drupal_add_html_head($tag_element, 'og_video_width');
		
			$tag_element = array(
				'#tag' => 'meta',
				'#attributes' => array(
					'property' => 'og:video:type',
					'content' => "application/x-shockwave-flash",
				),
			);
			drupal_add_html_head($tag_element, 'og_video_type');
			
			$tag_element = array(
				'#tag' => 'meta',
				'#attributes' => array(
					'property' => 'og:video',
					'content' => $video_url,
				),
			);
			drupal_add_html_head($tag_element, 'og_video');
			
		}
		
        if(!empty($item['video_file'])){
          $video = "<div class='flowplayer-video' style='display: block; width:" . $settings['video_width'] . "px; height:" . $settings['video_height'] . "px;' id='player_" . $delta .  "'></div>";
          $element[$delta]['#markup'] = $video;
          $element[$delta]['#markup'] = $element[$delta]['#markup'] . $flowplayer_js;
        }
        /**
        * Video Event Collection Result Rendering
        */
        if ($field['settings']['video_event_collection'] == 'Yes') {
          if (user_access('view video event statistics permissions')) {
            $headers = array(
              'name' => t('Name'),
              'count' => t('Count'),
            );
            $rows = array();
            $event_types = array('Start', 'Finish', 'Umcomplete');
            foreach ($event_types as $evenr_type) {
              $video_event_result = db_select('video_event', 'v')
                ->fields('v', array('event', 'eid', 'timecode'))
                ->condition('nid', $nid)
                ->condition('event', $evenr_type)
                ->execute()
                ->rowCount();
              $row = array(
                'name' => $evenr_type,
                'count' => $video_event_result,
              );
              $rows[] = $row;
            }
    
            $uncomplete_headers = array(
              'timecode' => t('Timecode'),
            );
    
            $uncomplete_rows = array();
            $video_uncomplete_event_result = db_select('video_event', 'v')
              ->fields('v', array('timecode'))
              ->condition('nid', $nid)
              ->condition('event', 'Umcomplete')
              ->execute()
              ->fetchAll();
            foreach ($video_uncomplete_event_result as $uncomple_event) {
              $uncomplete_row = array(
                'timecode' => msToTime($uncomple_event->timecode),
              );
              $uncomplete_rows[] = $uncomplete_row;
            }
            $element[$delta]['#markup'] = $element[$delta]['#markup'] . theme('table', array('header' => $headers, 'rows' => $rows));
            $element[$delta]['#markup'] = $element[$delta]['#markup'] . theme('table', array('header' => $uncomplete_headers, 'rows' => $uncomplete_rows));
            $element[$delta]['#markup'] = $element[$delta]['#markup'] . '<div id="chart_div"></div>';
          }
        }
      }
      break;
      
    case 'flowplayer_thumbnail':
      if(!empty($items[0]['video_thumbnail_image'])) {
        $video_thumbnail_file = file_load($items[0]['video_thumbnail_image']);
        if ($settings['thumbnail_image_style']) {
          $element[0]['#markup'] = theme('image_style', array('style_name' => $settings['thumbnail_image_style'], 'path' => $video_thumbnail_file->uri));  
        } else {
          $element[0]['#markup'] = '<img src="' . file_create_url($video_thumbnail_file->uri) . '"/>';
        }
      }
      break;
  }
 
  return $element;
}